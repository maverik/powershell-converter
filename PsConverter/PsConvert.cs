﻿namespace PsConverter
{
    using PsConverter.Common;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Management.Automation;
    using System.Management.Automation.Runspaces;

    public static class PsConvert
    {
        static readonly object LockObject = new object();
        static readonly Dictionary<Type, TypeMetadata> TypeCache = new Dictionary<Type, TypeMetadata>();

        public static PSObject SerializeToPsObject<T>(this T input) => input.Serialize().SerializeToPsObject();

        public static T Deserialize<T>(this PSObject psObject) where T : new() =>
            new T().Deserialize(psObject.Properties.ToDictionary(x => x.Name, x => x.Value));

        public static T Deserialize<T>(this T input, PSObject psObject) =>
            input.Deserialize(psObject.Properties.ToDictionary(x => x.Name, x => x.Value));

        public static PSObject SerializeToPsObject(this Dictionary<string, object> propertyBag)
        {
            var item = new PSObject();

            foreach (var (key, value) in propertyBag)
                item.Properties.Add(new PSNoteProperty(key, value));

            return item;
        }

        public static Dictionary<string, object> Serialize<T>(this T input)
        {
            object GetValueForPropertyOrField(PropertyWithMetadata x, T o) => x.HasGetter ? x.Property.GetValue(o) : x.BackingField.GetValue(o);
            object GetFinalValue(IValueConverter c, object v) => c?.Convert(v) ?? v;
            bool ItemValidForHydration(PropertyWithMetadata x) => !x.IsIgnored && (x.HasGetter || x.BackingField != null);

            return input.GetTypeMetadata().ItemsForHydration
                .Where(ItemValidForHydration)
                .ToDictionary(x => x.LookupName, x => GetFinalValue(x.ValueConverter, GetValueForPropertyOrField(x, input)));
        }

        public static T Deserialize<T>(this T input, Dictionary<string, object> propertyBag)
        {
            foreach (var item in input.GetTypeMetadata().ItemsForHydration.Where(x => !x.IsIgnored && propertyBag.ContainsKey(x.LookupName)))
            {
                var value = item.ValueConverter == null
                    ? propertyBag[item.LookupName]
                    : item.ValueConverter.ConvertBack(propertyBag[item.LookupName]);

                //for properties where we can set direct values, set them as such
                if (item.HasSetter)
                    item.Property.SetValue(input, value);
                else
                    item.BackingField.SetValue(input, value);
            }

            return input;
        }

        public static Command CreateSelectObjectPropertyCommand<T>(this T input) => CreateSelectObjectPropertyCommand<T>();

        public static Command CreateSelectObjectPropertyCommand<T>()
        {
            var metadata = GetTypeMetadata<T>();

            var command = new Command("Select-Object");
            command.Parameters.Add("Property", new ArrayList(metadata.ItemsForHydration.Select(x => x.LookupName).ToArray()));

            return command;
        }

        public static TypeMetadata GetTypeMetadata<T>(this T input) => GetTypeMetadata<T>();

        public static TypeMetadata GetTypeMetadata<T>()
        {
            var typeofT = typeof(T);
            TypeMetadata metadata;

            lock (LockObject)
                if (TypeCache.ContainsKey(typeofT))
                    metadata = TypeCache[typeofT];
                else
                {
                    metadata = new TypeMetadata(typeofT);
                    TypeCache[typeofT] = metadata;
                }

            return metadata;
        }
    }
}
