﻿namespace PsConverter
{
    using System;
    using System.Linq;
    using System.Reflection;


    public class TypeMetadata
    {
        static readonly Func<string, string>[] FieldNamePatterns =
        {
                x => $"<{x}>k__BackingField", //compiler generated backing field for named class
                x => $"<{x}>i__Field", //compiler generated backing field for anonymous class
                x => $"_{char.ToLowerInvariant(x[0])}{x.Substring(1)}", //_propertyName
                x => $"{char.ToLowerInvariant(x[0])}{x.Substring(1)}", //propertyName
                x => $"_{x}", //_PropertyName
            };

        public TypeMetadata(IReflect type)
        {
            var backingFieldLookup = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic).ToDictionary(x => x.Name, x => x);
            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            ItemsForHydration = properties
                .Select(x => new
                {
                    Property = x,
                    BackingFieldName = FieldNamePatterns.Select(generator => generator(x.Name))
                        .SingleOrDefault(backingFieldLookup.ContainsKey)
                })
                .Where(x => x.BackingFieldName != null)
                .Select(x => new PropertyWithMetadata(x.Property, backingFieldLookup[x.BackingFieldName]))
                .ToArray();
        }

        public PropertyWithMetadata[] ItemsForHydration { get; }
    }
}
