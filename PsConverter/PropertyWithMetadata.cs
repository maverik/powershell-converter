﻿namespace PsConverter
{
    using System.Reflection;
    using Common;

    public class PropertyWithMetadata
    {
        public PropertyWithMetadata(PropertyInfo propertyInfo, FieldInfo fieldInfo)
        {
            Property = propertyInfo;
            BackingField = fieldInfo;

            LookupName = propertyInfo.GetCustomAttribute<PsNameAttribute>()?.Name ?? propertyInfo.Name;
            ValueConverter = propertyInfo.GetCustomAttribute<ConvertUsingAttribute>()?.Converter;
            IsIgnored = propertyInfo.GetCustomAttribute<IgnoreHydrationAttribute>() != null;
        }

        public PropertyInfo Property { get; }
        public FieldInfo BackingField { get; }

        public bool HasGetter => Property.CanRead;
        public bool HasSetter => Property.CanWrite;

        public string LookupName { get; }
        public IValueConverter ValueConverter { get; }
        public bool IsIgnored { get; }
    }

}
