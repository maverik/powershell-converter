﻿namespace PsConverter.Common
{
    using System;

    public abstract class AbstractValueConverter<TSerialize, TDeserialize> : IValueConverter<TSerialize, TDeserialize>
    {
        object IValueConverter.Convert(object value) => Convert((TSerialize)value);

        object IValueConverter.ConvertBack(object value) => ConvertBack((TDeserialize)value);

        public virtual bool CanConvert(TSerialize value) => false;

        public virtual bool CanConvertBack(TDeserialize value) => false;

        public virtual TDeserialize Convert(TSerialize value) => throw new NotImplementedException();

        public virtual TSerialize ConvertBack(TDeserialize value) => throw new NotImplementedException();
    }
}