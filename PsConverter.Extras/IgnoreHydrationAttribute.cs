﻿namespace PsConverter.Common
{
    using System;
    
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class IgnoreHydrationAttribute : Attribute { }
}