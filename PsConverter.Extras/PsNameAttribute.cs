﻿namespace PsConverter.Common
{
    using System;

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class PsNameAttribute : Attribute
    {
        public PsNameAttribute(string name) => Name = name;

        public string Name { get; }
    }
}