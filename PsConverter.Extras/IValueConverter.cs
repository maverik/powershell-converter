﻿namespace PsConverter.Common
{
    public interface IValueConverter
    {
        object Convert(object value);
        object ConvertBack(object value);
    }

    public interface IValueConverter<TInput, TOutput> : IValueConverter
    {
        bool CanConvert(TInput value);
        TOutput Convert(TInput value);

        bool CanConvertBack(TOutput value);
        TInput ConvertBack(TOutput value);
    }
}