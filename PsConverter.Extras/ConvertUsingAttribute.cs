﻿namespace PsConverter.Common
{
    using System;

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class ConvertUsingAttribute : Attribute
    {
        static readonly Type ValueConverterType = typeof(IValueConverter);
        public ConvertUsingAttribute(Type converterType)
        {
            if (converterType.GetInterface(nameof(IValueConverter)) != null)
                Converter = Activator.CreateInstance(converterType) as IValueConverter;
        }

        public IValueConverter Converter { get; }
    }
}
