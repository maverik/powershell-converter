﻿// ReSharper disable All
namespace PsConverter.Tests
{
    using System;
    using Common;

    public class TestModel : IEquatable<TestModel>
    {
#pragma warning disable 0649
        long _readonlyBackingField;
#pragma warning restore 0649

        public Guid Id { get; set; }

        public long ReadonlyBackingField => _readonlyBackingField;

        public long ReadonlyAutoField { get; }

        [PsName("Unit")]
        public string UnitName { get; set; }

        [IgnoreHydration]
        public string IgnoredProperty { get; set; }

        [ConvertUsing(typeof(DoubleStringConverter))]
        public double Temperature { get; set; }

        public bool Equals(TestModel other) => 
            !(other is null) &&
            (ReferenceEquals(this, other) || Id.Equals(other.Id) &&
            ReadonlyAutoField == other.ReadonlyAutoField &&
            string.Equals(UnitName, other.UnitName,
                StringComparison.InvariantCulture) &&
            string.Equals(IgnoredProperty, other.IgnoredProperty,
                StringComparison.InvariantCulture) &&
            Temperature.Equals(other.Temperature));

        public override bool Equals(object obj) => 
            !(obj is null) &&
            (ReferenceEquals(this, obj) || obj.GetType() == typeof(TestModel) && Equals((TestModel)obj));

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id.GetHashCode();
                hashCode = (hashCode * 397) ^ ReadonlyAutoField.GetHashCode();
                hashCode = (hashCode * 397) ^ (UnitName != null ? StringComparer.InvariantCulture.GetHashCode(UnitName) : 0);
                hashCode = (hashCode * 397) ^ (IgnoredProperty != null ? StringComparer.InvariantCulture.GetHashCode(IgnoredProperty) : 0);
                hashCode = (hashCode * 397) ^ Temperature.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(TestModel left, TestModel right) => Equals(left, right);

        public static bool operator !=(TestModel left, TestModel right) => !Equals(left, right);
    }
}