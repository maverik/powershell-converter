namespace PsConverter.Tests
{
    using PsConverter.Common;
    using System.Globalization;

    public class DoubleStringConverter : AbstractValueConverter<double, string>
    {
        public static DoubleStringConverter Default { get; } = new DoubleStringConverter();

        public override bool CanConvertBack(string value) => double.TryParse(value, out _);

        public override double ConvertBack(string value) => double.Parse(value, CultureInfo.InvariantCulture);

        public override bool CanConvert(double value) => true;

        public override string Convert(double value) => value.ToString(CultureInfo.InvariantCulture);

    }
}