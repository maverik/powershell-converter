﻿namespace PsConverter.Tests
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.Linq;
    using System.Management.Automation;
    using NUnit.Framework;

    [TestFixture]
    public class PsConvertUsingStringDoubleTests
    {
        public PsConvertUsingStringDoubleTests() =>
            //Warm up cache
            PsConvert.GetTypeMetadata<TestModel>();

        [Test]
        public void PsConvertCanRoundtrip()
        {
            var ticks = DateTime.Now.Ticks;
            var input = new PSObject();
            
            input.Properties.Add(new PSNoteProperty(nameof(TestModel.ReadonlyAutoField), ticks));
            input.Properties.Add(new PSNoteProperty(nameof(TestModel.ReadonlyBackingField), ticks));
            
            var model = new TestModel {Id = Guid.NewGuid(), Temperature = 63.2d, UnitName = "F"}.Deserialize(input);
            
            var newModel = model.SerializeToPsObject().Deserialize<TestModel>();

            Assert.AreEqual(model, newModel);
        }

        [Test]
        public void ConvertUsingConverterSetsTemperature()
        {
            var input = new PSObject();
            const double value = 63.2d;

            input.Properties.Add(new PSNoteProperty(nameof(TestModel.Temperature), value.ToString(CultureInfo.InvariantCulture)));

            var model = input.Deserialize<TestModel>();

            Assert.AreEqual(value, model.Temperature);
        }

        [Test]
        public void PsNameOverridePropertyName()
        {
            var input = new PSObject();
            const string value = "F";

            input.Properties.Add(new PSNoteProperty("Unit", value));

            var model = new TestModel().Deserialize(input);

            Assert.AreEqual(value, model.UnitName);
        }

        [Test]
        public void IgnoreHydrationIsHonored()
        {
            var input = new PSObject();
            const string value = "Ignored";

            input.Properties.Add(new PSNoteProperty(nameof(TestModel.IgnoredProperty), value));

            var model = new TestModel().Deserialize(input);

            Assert.AreNotEqual(value, model.IgnoredProperty);
        }

        [Test]
        public void PublicSettablePropertyIsHydrated()
        {
            var input = new PSObject();
            var value = Guid.NewGuid();

            input.Properties.Add(new PSNoteProperty(nameof(TestModel.Id), value));

            var model = new TestModel().Deserialize(input);

            Assert.AreEqual(value, model.Id);
        }

        [Test]
        public void PublicReadonlyAutoPropertyIsHydrated()
        {
            var input = new PSObject();
            var value = DateTime.Now.Ticks;

            input.Properties.Add(new PSNoteProperty(nameof(TestModel.ReadonlyAutoField), value));

            var model = new TestModel().Deserialize(input);

            Assert.AreEqual(value, model.ReadonlyAutoField);
        }

        [Test]
        public void PublicReadonlyBackingPropertyIsHydrated()
        {
            var input = new PSObject();
            var value = DateTime.Now.Ticks;

            input.Properties.Add(new PSNoteProperty(nameof(TestModel.ReadonlyBackingField), value));

            var model = new TestModel().Deserialize(input);

            Assert.AreEqual(value, model.ReadonlyBackingField);
        }

        [Test]
        public void SelectObjectPropertyCommandReturnsAllViablePropertiesOnly()
        {
            var propertiesForHydration = new ArrayList(new TypeMetadata(typeof(TestModel)).ItemsForHydration.Select(x => x.LookupName).ToArray());
            var command = PsConvert.CreateSelectObjectPropertyCommand<TestModel>();

            Assert.AreEqual(propertiesForHydration, command.Parameters[0].Value);
        }
    }
}
