namespace PsConverter.Tests
{
    using System;
    using System.Globalization;
    using NUnit.Framework;

    [TestFixture]
    public class AbstractValueConverterUsingDoubleStringConverterTests
    {
        public static double TypedValue { get; } = Math.PI;

        public static string StringValue { get; } = Math.PI.ToString("F14", CultureInfo.InvariantCulture);

        [Test]
        public void ConverterCanConvert()
        {
            var converter = DoubleStringConverter.Default;
            Assert.IsTrue(converter.CanConvert(TypedValue));
            Assert.AreEqual(StringValue, converter.Convert(TypedValue));
        }

        [Test]
        public void ConverterCanConvertBack()
        {
            var converter = DoubleStringConverter.Default;
            Assert.IsTrue(converter.CanConvertBack(StringValue));
            Assert.AreEqual(TypedValue, converter.ConvertBack(StringValue), double.Parse("1E-14"));
        }
    }
}
